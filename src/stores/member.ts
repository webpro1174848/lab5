import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        {id:1, name: 'มานะ รักชาติ',tel: '0881234567'},
        {id:2, name: 'มานะ ชู',tel: '0689562558'}
    ])
    const currrentMember = ref<Member | null>()
    const searchMember = (tel:string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if(index < 0) {
            currrentMember.value = null
        }
        currrentMember.value = members.value[index]
    }
    function clear(){
        currrentMember.value=null
    }
    return { members,currrentMember,searchMember,clear}
})
